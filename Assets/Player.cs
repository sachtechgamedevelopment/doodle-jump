using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody2D))]
public class Player : MonoBehaviour {

	public float movementSpeed = 10f;
    public Text mtext;
	Rigidbody2D rb;
    bool isGameOne = true;

	float movement = 0f;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        if (isGameOne)
        {
            movement = Input.GetAxis("Horizontal") * movementSpeed;
            if (Input.touchCount > 0)
            {
                Touch iTouch = Input.GetTouch(0);
                float touchX = iTouch.position.x;
                float screenCenterX = Screen.width * 0.5f;
                if (touchX > screenCenterX)
                {
                    movement = movementSpeed/5;
                }
                else
                {
                    movement = -1 * movementSpeed/5;
                }
            }
        }
	}
    private void OnBecameInvisible()
    {
        mtext.text = "Game over";
        isGameOne = false;
        StartCoroutine(Restart());

    }
    IEnumerator Restart()
    {
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }

    void FixedUpdate()
	{
        if (isGameOne)
        {
            Vector2 velocity = rb.velocity;
            velocity.x = movement;
            rb.velocity = velocity;
        }
	}
}
